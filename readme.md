
#  BitDiffer

BitDiffer is a GUI and Command Line application to compare versions of .NET assemblies.

For the latest executables, go to the [Download Page](https://bitbucket.org/grennis/bitdiffer-win/downloads).

For the full set of documentation browse the [Wiki Page](https://bitbucket.org/grennis/bitdiffer-win/wiki/).
